import json
import socket

from typing import Dict, Any, List
from colorama import Fore, Style


def read_json(file_path: str) -> Dict[str, Any]:
    with open(file_path, 'r') as file:
        data = json.load(file)
    return data

def get_id_addresses(domain_name: str) -> List[str]:
    try:
        return socket.gethostbyname_ex(domain_name)[-1]
    except socket.gaierror as error:
        print(f'Ошибка при получении адресов хоста для {domain_name}: {error}')

def detect_changes(domain_data: List[Dict[str, Any]]) -> Dict[str, List[str]]:
    changed_values = {}

    for domain in domain_data:
        changed_ip = []
        ip_address = get_id_addresses(domain['domainName'])

        for ia in ip_address:
            if ia not in domain['ipAddress']:
                print(Fore.CYAN + f'Домен {domain["domainName"]} IP изменился на {ia}' + Style.RESET_ALL)
                changed_ip.append(ia)

        if len(changed_ip) > 0:
            changed_values[domain['domainName']] = changed_ip
            
    if len(changed_values) == 0:
        print('Изменений нет')

    return changed_values

def update_domain_data(initial_data: Dict[str, Any], detect_changes: Dict[str, List[str]]) -> Dict[str, Any]:
    for domain in initial_data['domains']:
        domain_name = domain['domainName']
        if domain_name in detect_changes:
            domain['ipAddress'] = detect_changes[domain_name]

    return initial_data

def write_json(file_path: str, data: Dict[str, Any]) -> None:
    with open(file_path, 'w', encoding='utf-8') as file:
        json.dump(data, file, ensure_ascii=False, indent=4)

def main() -> None:
    file_path = 'addresses.json'
    initial_data = read_json(file_path)
    changes = detect_changes(initial_data['domains'])
    updated_data = update_domain_data(initial_data, changes)
    write_json(file_path, updated_data)

if __name__ == '__main__':
    main()
